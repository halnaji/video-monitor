import cv2
import numpy as np;

capture = cv2.VideoCapture(0)

while True:
    ret, image = capture.read()
    # image1 = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    cv2.imshow('frame', image)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break


capture.release()
cv2.destroyAllWindows()